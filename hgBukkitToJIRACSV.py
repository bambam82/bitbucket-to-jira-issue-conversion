#!/usr/bin/env python
# encoding: utf-8
"""
untitled.py

Created by Jonathan Saggau on 2012-11-24.
Copyright (c) 2012 Enharmonic, inc. All rights reserved.
"""

import sys
import os
import getopt
import json
import csv
import codecs
import StringIO
import re

help_message = '''
-i inputfile.json -o outputfile.csv
'''



class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg

class ProcessJSON(object):
    inputFile = None
    outputFile = None
    csvLines = u''
    keyMapBukkitToJIRA = {
        u'status':u'Status',
        u'content':u'Description',
        u'title':u'Item name',
        u'component':u'Component',
        u'reporter.display_name':u'Reporter',
        # u'updated_on':u'Updated',
        u'created_on':u'Start date',
        u'priority':u'Priority',
        # u'watchers.display_name':u'Watchers',
        # u'metadata.component':u'Labels',
        u'kind':u'Item Type',
        u'assignee.display_name':u'Item owner',
        u'id':u'Item id',
        u'url':u'url',
        u'version':u'version'
    }

    def process(self, inFile, outputFile):
        self.inputFile = inFile;
        self.outputFile = outputFile;
        self._read()
        self._write()

    def _arrayToCSV(self, arrayToConvert, quotechar='\"', quoting=csv.QUOTE_NONNUMERIC):
        fakeFile = StringIO.StringIO()
        writer = csv.writer(fakeFile, delimiter=',',
                            quotechar=quotechar, quoting=quoting)
        # for s in arrayToConvert:
        #     print "s:", s
        writer.writerow([s.encode("utf-8") for s in arrayToConvert]);
        outputString = fakeFile.getvalue()
        fakeFile.close()
        return outputString

    def _headerLineOfCSV(self, representativeInDict):
        csvLineArray = self.keyMapBukkitToJIRA.values();
        return self._arrayToCSV(csvLineArray, quotechar='', quoting=csv.QUOTE_NONE)

    def _singleLineOfCSV(self, inDict):
        csvLineArray = [];

        priority = {
                'trivial': 'None',
                'minor': 'Low',
                'major': 'Medium',
                'critical': 'High',
                'blocker': 'High',
        }
        months = {
            '01':'Jan',
            '02':'Feb',
            '03':'Mar',
            '04':'Apr',
            '05':'May',
            '06':'Jun',
            '07':'Jul',
            '08':'Aug',
            '09':'Sep',
            '10':'Oct',
            '11':'Nov',
            '12':'Dec'
        }

        for bitBukkitKey in self.keyMapBukkitToJIRA.keys():
            # print "bitBukkitKey:", bitBukkitKey
            JIRAKey = self.keyMapBukkitToJIRA[bitBukkitKey]
            # print "JIRAKey:", JIRAKey
            try:
                bitBukkitValue = inDict[bitBukkitKey]
                # print "bitBukkitValue:", bitBukkitValue
                # if (bitBukkitKey is u'title'
                #     and (bitBukkitValue is u'' or bitBukkitValue is None)):
                if (bitBukkitValue is u'' or bitBukkitValue is None):
                    # bitBukkitValue = u'(Empty)'
                    bitBukkitValue = u''
            except KeyError, exception:
                try:
                    # print "bitBukkitKey:", bitBukkitKey
                    # print "bitBukkitValue:", bitBukkitValue
                    keys = bitBukkitKey.split(u'.')
                    # print "2 keys:", keys, type(keys)
                    bitBukkitValue = inDict[keys[0]][keys[1]]
                    # print "2 bitBukkitValue:", bitBukkitValue, type(bitBukkitValue)
                except KeyError, exception:
                    # bitBukkitValue = u'(Empty)'
                    bitBukkitValue = u''
                except TypeError, exception:
                    # bitBukkitValue = u'(Empty)'
                    bitBukkitValue = u''
            if type(bitBukkitValue) is int:
                bitBukkitValue = u'%s' % str(bitBukkitValue)
            # print "bitBukkitValue: '%s' %s " % (bitBukkitValue, type(bitBukkitValue))

            # Translations and specials
            if bitBukkitKey == u'title':
                bitBukkitValue = '#%s %s' % (inDict[u'id'], bitBukkitValue)
            elif bitBukkitKey == u'created_on':
                # print "bitBukkitValue:", bitBukkitValue
                # bitBukkitValue = re.sub(r"(\d+)-(\d+)-(\d+)T(\d+):(\d+).*", r"\1/%s/\3 \4:\5" % months[\2], bitBukkitValue)
                # bitBukkitValue = re.sub(r"(\d+)-(\d+)-(\d+)T(\d+):(\d+).*", r"%s/%s/%s %s:%s" % (\1, months[\2], \3, \4, \5), bitBukkitValue)
                r = re.search(r"(\d+)-(\d+)-(\d+)T(\d+):(\d+).*", bitBukkitValue)
                bitBukkitValue = r"%s/%s/%s %s:%s" % (r.group(1), months[str(r.group(2))], r.group(3), r.group(4), r.group(5))
                # print "bitBukkitValue2:", bitBukkitValue
            elif bitBukkitKey == u'priority':
                bitBukkitValue = priority.get(bitBukkitValue) or bitBukkitValue
            elif bitBukkitKey == u'url':
                bitBukkitValue = 'https://bitbucket.org/netyce/yce7/issues/%s/' % inDict[u'id']
            elif bitBukkitKey == u'content':
                # print "urls: ", bitBukkitKey
                urls = [att['url'] for att in attachments if att['issue'] == inDict[u'id']]
                bitBukkitValue = bitBukkitValue + '\n'.join(urls)
                if bitBukkitValue:
                    bitBukkitValue = '\n\n' + bitBukkitValue

            csvLineArray.append(bitBukkitValue)
        return self._arrayToCSV(csvLineArray)

    def _read(self):
        """Reads and parses the input file"""
        global attachments

        fileHandle = codecs.open(self.inputFile, encoding='utf-8')
        alljson = json.load(fileHandle)
        issues = alljson[u'issues']
        attachments = alljson[u'attachments']

        print "issues count = %s" % len(issues)
        line = self._headerLineOfCSV(issues[0])
        # self.csvLines += line + os.linesep
        self.csvLines += line
        notparsing = ('resolved', 'closed', 'wontfix', 'invalid')
        openissues = [eachIssue for eachIssue in issues if eachIssue['status'] not in notparsing]
        print "open issues count = %s" % len(openissues)

        # for eachIssue in issues:
        for eachIssue in openissues:
            # print "eachIssue:", eachIssue
            line = self._singleLineOfCSV(eachIssue)
            # print "line:", line
            # self.csvLines += line.decode('utf-8') + os.linesep
            self.csvLines += line.decode('utf-8')
        fileHandle.close()

    def _write(self):
        """Writes the output file"""
        try:
            someFile = codecs.open(self.outputFile, 'w', encoding='utf-8')
            someFile.write(self.csvLines)
            someFile.close()
        except Exception as exc:
            print('Error writing %s:%s' %(self.outputFile, exc))
            print
            print

def main(argv=None):
    inFile = None
    outFile = None

    if argv is None:
        argv = sys.argv
    try:
        try:
            opts, args = getopt.getopt(argv[1:], "ho:i:", ["help", "output=", "input="])
        except getopt.error, msg:
            raise Usage(msg)

        # option processing
        for option, value in opts:
            if option in ("-h", "--help"):
                raise Usage(help_message)
            if option in ("-o", "--output"):
                outFile = value
            if option in ("-i", "--input"):
                inFile = value
        processor = ProcessJSON()
        if (inFile is None or outFile is None):
            raise Usage(help_message)
        else:
            processor.process(inFile, outFile)

    except Usage, err:
        print >> sys.stderr, sys.argv[0].split("/")[-1] + ": " + str(err.msg)
        print >> sys.stderr, "\t for help use --help"
        return 2


if __name__ == "__main__":
    sys.exit(main())
