###Basic instructions to be fleshed-out at a later date

#1. Download all issues from the Bitbucket issue tracker
- Example Command: 
        curl --user yourusername@domain.tld:$PASSWORD "https://api.bitbucket.org/1.0/repositories/Enharmonic/bitbucket-to-jira-issue-conversion/issues?limit=50" > issues.json

- if you have more than 50 issues, you'll have to make multiple requests.
- Docs: 
        https://confluence.atlassian.com/display/BITBUCKET/issues+Resource
        https://confluence.atlassian.com/display/BITBUCKET/issues+Resource#issuesResource-GETalistofissuesinarepository'stracker

#2. Replace any non-ASCII characters in the JSON 
- Typically smart quotes.
- I think it's a problem with the memory-mapped files in python that causes UTF-8 not to work.

#3. Run the python script to make a csv of the issues
- Command:
        hgBukkitToJIRACSV.py -i ./issues.json -o ./issues.csv

#4. import the issues.csv into JIRA
- https://confluence.atlassian.com/display/JIRA/Importing+Data+from+CSV
- See images in the docs directory for some screenshots
